## Project Overview
#####  - Simple Building App.

### How to run it:
  1. First clone th project from the repository
  2. Open a terminal in the root directory and run ``` npm install``` in order to install all needed dependencies
  3. Then run  ``` npm run start ```

### Technologies used:
  - React, Redux, JS, HTML, Sass(CSS), React Router, immer, Reselect, Saga, Ramda