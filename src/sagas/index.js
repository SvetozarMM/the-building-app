import { takeEvery } from 'redux-saga/effects';
import { FETCH_BUILDINGS_DATA } from '../constants/buildings';
import { onFetchBuildingsData } from './buildings';

export default function* watchSagas() {
    yield takeEvery(FETCH_BUILDINGS_DATA, onFetchBuildingsData);
}
