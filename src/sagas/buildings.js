import { put, select } from 'redux-saga/effects';
import * as BuildingsService from '../services/buildings-service';
import { saveBuildingsData } from '../actions/buildings';

export function* onFetchBuildingsData() {
    const isDataFetched = yield select((state) => state.buildings.isDataFetched);

    if (isDataFetched) {
        return;
    }

    try {
        const data = yield BuildingsService.getAllBuildingRecords();

        if (data) {
            yield put(saveBuildingsData(data));
        }
    } catch (error) {
        yield console.log(`Error!, ${error}`);
    }
}
