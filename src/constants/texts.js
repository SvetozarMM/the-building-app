export const PAGE_HOME_DESCRIPTION = `" If there is a place of charms, this is it, it’s here "`;
export const PAGE_BUILDINGS= 'Buildings';
export const PAGE_HOME = 'Home';
export const COPYRIGHT_TEXT = `BUILDING APP - Svetozar Martinov`;
export const WELCOME_TEXT = 'Welcome';
export const DELETE_MESSAGE = 'Are you sure to delete this record?'
export const NO_RECORDS = 'No records found';
export const USERNAME = 'John Doe';
