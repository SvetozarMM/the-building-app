export const FETCH_BUILDINGS_DATA = 'FETCH_BUILDINGS_DATA';
export const SAVE_BUILDINGS_DATA = 'SAVE_BUILDINGS_DATA';
export const ADD_BUILDING = 'ADD_BUILDING';
export const UPDATE_BUILDING = 'UPDATE_BUILDING';
export const DELETE_BUILDING = 'DELETE_BUILDING';
export const SET_SELECTED_BUILDING = 'SET_SELECTED_BUILDING';

export const BUILDING_INPUTS = {
    name: {
        type: 'text',
        placeholder: 'Name',
        errorMessage: 'Name must be at least 3 characters!',
        isEditable: true
    },
    area: {
        type: 'number',
        placeholder: 'Area',
        errorMessage: 'It`s necessary to enter an area value.',
        isEditable: true
    },
    location: {
        type: 'text',
        placeholder: 'Location',
        errorMessage: 'Location must be at least 3 characters!'
    }
};

export const UNAVAILABLE_NAME_MESSAGE = 'The name is already taken.';
