export const TABLE_CELLS = [
    {
        cellId: 'imageSrc',
        label: 'logo'
    },
    {
        cellId: 'name',
        label: 'name'
    },
    {
        cellId: 'area',
        label: 'area'
    },
    {
        cellId: 'location',
        label: 'location'
    },
    {
        cellId: 'id',
        label: 'id'
    }
];

