import {
  FETCH_BUILDINGS_DATA,
  SAVE_BUILDINGS_DATA,
  ADD_BUILDING,
  UPDATE_BUILDING,
  DELETE_BUILDING,
  SET_SELECTED_BUILDING,
} from "../constants/buildings";

export const fetchBuildingsData = () => ({
  type: FETCH_BUILDINGS_DATA,
});

export const saveBuildingsData = (buildings) => ({
  type: SAVE_BUILDINGS_DATA,
  payload: { buildings },
});

export const addBuilding = (data) => ({
  type: ADD_BUILDING,
  payload: { data },
});

export const updateBuilding = ({ buildingId, updatedData }) => ({
  type: UPDATE_BUILDING,
  payload: { buildingId, updatedData },
});

export const deleteBuilding = (id) => ({
  type: DELETE_BUILDING,
  payload: { id },
});

export const setSelectedBuilding = (id) => ({
    type: SET_SELECTED_BUILDING,
    payload: { id },
});
