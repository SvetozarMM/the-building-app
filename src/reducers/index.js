import { combineReducers } from 'redux';
import buildings from './buildings';

export default combineReducers({
    buildings
});