import { produce } from 'immer';
import { pluck, filter } from 'ramda';
import {
  SAVE_BUILDINGS_DATA,
  ADD_BUILDING,
  UPDATE_BUILDING,
  DELETE_BUILDING,
  SET_SELECTED_BUILDING
} from "../constants/buildings";
import { randomIdGenerator, getRandomImage } from '../utils/buildings';

const initialState = {
    buildingsIds: [],
    buildingsById: {},
    isDataFetched: false,
    selectedBuildingId: '',
    setIsCreateMode: false
};

const buildingInitialData = {
    id: '',
    name: '',
    area: '',
    location: '-',
    imageSrc: ''
}

export default produce((draftState, { type, payload } = {}) => {
    switch (type) {
        case SAVE_BUILDINGS_DATA: {
            const { buildings } = payload;

            draftState.buildingsIds = pluck('id', buildings);
            draftState.buildingsById = buildings.reduce((acc, item) =>
                acc = {
                    ...acc,
                    [item.id]: item 
                }, {});
            draftState.isDataFetched = true;

            return;
        }
        case ADD_BUILDING: {
            const { data } = payload;
            const id = randomIdGenerator();

            draftState.buildingsById[id] = {
                ...buildingInitialData,
                ...data,
                imageSrc: getRandomImage()
            };
            draftState.buildingsIds.unshift(id);

            return;
        }
        case UPDATE_BUILDING: {
            const { buildingId, updatedData } = payload;

            draftState.buildingsById[buildingId] = {
                ...draftState.buildingsById[buildingId],
                ...updatedData
            }

            return;
        }
        case DELETE_BUILDING: {
            const { id } = payload;

            draftState.selectedBuildingId = '';
            draftState.buildingsIds = filter(item => item !== id, draftState.buildingsIds);
            delete draftState.buildingsById[id];

            return;
        }
        case SET_SELECTED_BUILDING: {
            const { id } = payload;

            draftState.selectedBuildingId = id;

            return;
        }

        default: return draftState;
    }
}, initialState);
