import { connect } from 'react-redux';
import TableRow from '../../../../components/buildings/table-container/table-row/table-row';
import { setSelectedBuilding } from '../../../../actions/buildings';

export default connect((state, { id }) => {
    const { name, area, location, imageSrc } = state.buildings.buildingsById[id];

    return {
        name,
        area,
        location,
        imageSrc,
        isSelected: id === state.buildings.selectedBuildingId
    };
}, {
    setSelectedBuilding
})(TableRow);
