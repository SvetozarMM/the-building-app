import { connect } from 'react-redux';
import { setSelectedBuilding } from '../../../actions/buildings';
import { getAllBuildings } from '../../../selectors/buildings';
import TableContainer from '../../../components/buildings/table-container/table-container';

export default connect((state) => ({
    buildingsIds: getAllBuildings(state),
    selectedBuildingId: state.buildings.selectedBuildingId
}), {
    setSelectedBuilding
})(TableContainer);
