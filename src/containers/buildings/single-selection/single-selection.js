import { connect } from 'react-redux';
import { deleteBuilding, addBuilding } from '../../../actions/buildings'
import SingleSelection from '../../../components/buildings/single-selection/single-selection';

export default connect((state) => {
    const buildingId = state.buildings.selectedBuildingId;
    const building =  buildingId && state.buildings.buildingsById[buildingId];

    return {
        buildingId,
        name: (building && building.name) || '',
        area: (building && building.area) || '',
        location: (building && building.location) || '',
        imageSrc: (building && building.imageSrc) || ''
    }
}, {
    deleteBuilding,
    addBuilding
})(SingleSelection);
