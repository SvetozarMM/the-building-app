import { connect } from 'react-redux';
import InputBox from '../../../components/common/input-box/input-box';
import { updateBuilding } from '../../../actions/buildings';
import { getUnavailableNames } from '../../../selectors/buildings';

export default connect((state) => ({
    unavailableNames: getUnavailableNames(state)
}), {
    updateBuilding
})(InputBox);
