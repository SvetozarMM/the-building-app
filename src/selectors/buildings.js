import { createSelector } from 'reselect'
import { map, toLower } from 'ramda';

export const getUnavailableNames = createSelector(
    ({ buildings }) => buildings.buildingsById,
    ({ buildings }) => buildings.buildingsIds,
    (buildingsById, buildingsIds) => map((id) => toLower(buildingsById[id].name), buildingsIds)
);

export const getAllBuildings = createSelector(
    ({ buildings }) => buildings.buildingsIds,
    (buildingsIds) => buildingsIds
);
