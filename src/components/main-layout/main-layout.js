import React, { memo } from 'react';
import { Route, Redirect } from "react-router-dom";
import styles from './main-layout.module.scss';
import { LINK_PAGE_HOME, LINK_PAGE_BUILDINGS } from '../../constants/links';
import Home from '../home/home';
import Buildings from '../buildings/buildings';

const MainLayout = memo(() => {
    return (
        <div className={styles['main-layout']}>
            <Route path={LINK_PAGE_HOME} component={Home} />
            <Route path={LINK_PAGE_BUILDINGS} component={Buildings} />
            <Redirect from='/' to={LINK_PAGE_HOME} />
        </div>
    );
});

export default MainLayout;
