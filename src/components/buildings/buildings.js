import React, { useEffect, memo } from 'react';
import { connect } from 'react-redux';
import styles from './buildings.module.scss';
import { fetchBuildingsData } from '../../actions/buildings';
import TableContainer from '../../containers/buildings/table-container/table-container';
import SingleSelection from '../../containers/buildings/single-selection/single-selection';

const Buildings = memo((props) => {
    const { isLoading, fetchBuildingsData } = props;

    useEffect(() => {
        fetchBuildingsData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className={styles['buildings-container']}>
            {isLoading ? <p className={styles['loader']}>LOADING...</p> :
                <div className={styles['table-container']}>
                    <TableContainer />
                    <SingleSelection />
                </div>
            }
        </div>
    );
});

Buildings.defaultProps = {
    isLoading: false,
    fetchBuildingsData: () => {}
};

export default connect((state) => ({
    isLoading: !state.buildings.isDataFetched
}), {
    fetchBuildingsData
})(Buildings);
