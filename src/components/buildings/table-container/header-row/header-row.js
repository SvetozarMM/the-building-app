import React, { useCallback, memo } from 'react';
import classnames from 'classnames';
import { map } from 'ramda';
import styles from './hader-row.module.scss';
import { TABLE_CELLS } from '../../../../constants/table';

const HaderRow = memo(() => {
    const renderCells = useCallback(({ cellId, label }) =>
        <div 
            key={cellId}
            className={classnames(styles['haeder-cell'], {
                [styles[label]]: label
            })}
        >
            <p className={styles['label']}>{label}</p>
        </div>
    , []);

    return (
        <div className={styles['haeder-row']}>
            {map(renderCells, TABLE_CELLS)}
        </div>
    );
});

export default HaderRow;
