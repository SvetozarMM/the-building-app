import React, { useCallback, useEffect, memo } from 'react';
import { map } from 'ramda';
import styles from './table-container.module.scss';
import HaderRow from './header-row/header-row';
import TableRow from '../../../containers/buildings/table-container/table-row/table-row';
import { NO_RECORDS } from '../../../constants/texts';

const TableContainer = memo((props) => {
    const { selectedBuildingId, buildingsIds, setSelectedBuilding } = props;

    useEffect(() => {
        if (!selectedBuildingId) {
            setSelectedBuilding(buildingsIds[0]);
        }
    }, [selectedBuildingId, buildingsIds, setSelectedBuilding]);

    const renderRows = useCallback((id) =>
        <TableRow key={id} id={id} />
    , []);

    return (
        <div className={styles['table-box']}>
            <HaderRow />
            <div className={styles['list']}>
                {map(renderRows, buildingsIds)}
                {selectedBuildingId ? null : <p className={styles['no-records']}>{NO_RECORDS}</p>}
            </div>
        </div>
    );
});

TableContainer.defaultProps = {
    selectedBuildingId: '',
    buildingsIds: [],
    setSelectedBuilding: () => {}
};

export default TableContainer;
