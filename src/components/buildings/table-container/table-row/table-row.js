import React, { useCallback, memo } from 'react';
import classnames from 'classnames';
import { cond, map, equals } from 'ramda';
import styles from './table-row.module.scss';
import { TABLE_CELLS } from '../../../../constants/table';

const TableRow = memo((props) => {
    const { id, name, area, location, imageSrc, isSelected, setSelectedBuilding } = props;

    const selectBuilding = useCallback(() => {
        setSelectedBuilding(id);
    }, [setSelectedBuilding, id]);

    const renderCell = useCallback((label, prop) =>
        <div
            key={label}
            className={classnames(styles['table-cell'], {
                [styles[label]]: label
            })}
        >
            <p className={styles['label']}>{prop}</p>
        </div>
    , []);

    const renderCells = useCallback(({ cellId, label }) =>
        cond([
            [equals('imageSrc'), () => <img key={id} className={styles['avatar']} src={`/images/${imageSrc}`} alt="avatar" />],
            [equals('name'), () => renderCell(label, name)],
            [equals('area'), () => renderCell(label, area)],
            [equals('location'), () => renderCell(label, location)],
            [equals('id'), () => renderCell(label, id)]
        ])(cellId)
    , [id, name, area, location, imageSrc, renderCell]);

    return (
        <div
            className={classnames(styles['table-row'], {
                [styles['selected']]: isSelected
            })} 
            onClick={selectBuilding}
        >
            {map(renderCells, TABLE_CELLS)}
        </div>
    );
});

TableRow.defaultProps = {
    id: '',
    name: '',
    area: '',
    location: '',
    imageSrc: '',
    isSelected: false,
    setSelectedBuilding: () => {}
};

export default TableRow;
