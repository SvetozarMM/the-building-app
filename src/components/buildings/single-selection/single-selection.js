import React, { useCallback, useEffect, useState } from 'react';
import classnames from 'classnames';
import styles from './single-selection.module.scss';
import InputBox from '../../../containers/common/input-box/input-box';
import { Icon } from '@material-ui/core';
import { DeleteOutlineOutlined, HomeWork } from '@material-ui/icons';
import { DELETE_MESSAGE } from '../../../constants/texts';
import AddBuildingBtn from '../../common/add-building-btn/add-building-btn';

const SingleSelection= (props) => {
    const { buildingId, name, area, location, imageSrc, deleteBuilding, addBuilding } = props;
    const [isDeleteMsgOpen, setIsDeleteMsgOpen] = useState(false);
    const [isCreateMode, setIsCreateMode] = useState(false);
    const [isInputsValid, setIsInputsValid] = useState(true);
    const [inputsValues, setInputsValues] = useState({});

    useEffect(() => {
        setIsCreateMode(!buildingId);
    }, [buildingId]);

    const onDeleteHandler = useCallback(() => {
        setIsDeleteMsgOpen(true);
    }, []);

    const onDeleteItem = useCallback(() => {
        deleteBuilding(buildingId);
        setIsDeleteMsgOpen(false);
    }, [deleteBuilding, buildingId]);

    const onCancelDeleteItem = useCallback(() => {
        setIsDeleteMsgOpen(false);
    }, []);

    const onSetCreateMode = useCallback(() => {
        setIsCreateMode(true);
    }, []);

    const onValidateInputs = useCallback((isValid) => {
        setIsInputsValid(isValid);
    }, []);

    const onSaveInputsValues = useCallback(({ buildingProp, value }) => {
        setInputsValues({
            ...inputsValues,
            [buildingProp]: value
        });
    }, [inputsValues]);

    const onCancelCreateHandler = useCallback(() => {
        setIsCreateMode(false);
        setInputsValues({});
    }, []);

    const onCreateRecord = useCallback(() => {
        if (isInputsValid && inputsValues['name'] && inputsValues['area']) {
            addBuilding(inputsValues);
            setIsCreateMode(false);
            setInputsValues({});
        }
    }, [addBuilding, isInputsValid, inputsValues]);

    return (
        <div className={styles['single-selection-container']}>
            {isCreateMode ?
                <Icon className={styles['avatar']} component={HomeWork} /> :
                <img className={styles['avatar']} src={`/images/${imageSrc}`} alt="avatar" />
            }

            <div className={styles['content']}>
                <div className={styles['inputs-container']}>
                    { !isCreateMode ?
                        <>
                            <div className={styles['inputs']}>
                                <InputBox buildingProp={'name'} initialValue={name} buildingId={buildingId} />
                                <InputBox buildingProp={'area'} initialValue={area} buildingId={buildingId} />
                                <InputBox buildingProp={'location'} initialValue={location} buildingId={buildingId} />
                            </div>
                            {isDeleteMsgOpen ?
                                <div className={styles['delete-message']}>
                                    <p className={styles['delete-text']}>{DELETE_MESSAGE}</p>
                                    <div className={styles['action-buttons']}>
                                        <p className={styles['btn-save']} onClick={onDeleteItem}>Yes</p>
                                        <p className={styles['btn-cancel']} onClick={onCancelDeleteItem}>Cancel</p>
                                    </div>
                                </div> : null
                            }
                            <div className={styles['actions']} onClick={onDeleteHandler}>
                                <Icon className={styles['icon']} component={DeleteOutlineOutlined} />
                                <p className={styles['label']}>Delete</p>
                            </div>
                        </>
                    :
                        <div className={styles['add-record-inputs']}>
                            <InputBox
                                buildingProp={'name'}
                                isValidated={onValidateInputs}
                                getValue={onSaveInputsValues}
                                isCreateMode={isCreateMode}
                            />
                            <InputBox
                                buildingProp={'area'}
                                isValidated={onValidateInputs}
                                getValue={onSaveInputsValues}
                                isCreateMode={isCreateMode}
                            />
                            <InputBox
                                buildingProp={'location'}
                                isValidated={onValidateInputs}
                                getValue={onSaveInputsValues}
                                isCreateMode={isCreateMode}
                            />
                        </div>
                    }
                </div>
                <div
                    className={classnames(styles['btn-box'], {
                        [styles['disabled']]: !(isInputsValid && inputsValues['name'] && inputsValues['area'])
                    })}
                >
                    {isCreateMode ?
                        <div className={styles['add-record-actions']}>
                            <p className={styles['btn-add']} onClick={onCreateRecord}>Add</p>
                            {!!buildingId ? <p className={styles['btn-cancel']} onClick={onCancelCreateHandler}>Cancel</p> : null}
                        </div> : <AddBuildingBtn onClick={onSetCreateMode} />
                    }
                </div>
            </div>
        </div>
    );
}

SingleSelection.defaultProps = {
    buildingId: '',
    name: '',
    area: '',
    location: '',
    imageSrc: '',
    deleteBuilding: () => {},
    addBuilding: () => {}
};

export default SingleSelection;
