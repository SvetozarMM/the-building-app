import React from 'react';
import MainLayout from './main-layout/main-layout';
import Header from './header/header';
import Footer from './footer/footer';

const App = () => {
    return (
        <div className="app">
            <Header />
            <MainLayout />
            <Footer />
        </div>
    );
}

export default App;
