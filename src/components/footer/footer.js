import React, { memo } from 'react';
import styles from './footer.module.scss';
import { PAGE_HOME, PAGE_BUILDINGS, COPYRIGHT_TEXT } from '../../constants/texts';
import { LINK_PAGE_HOME, LINK_PAGE_BUILDINGS } from '../../constants/links';
import Link from '../common/link/link';

const Footer = memo((props) => {
    return (
        <footer className={styles['footer']}>
            <div className={styles['navigation']}>
                <Link text={PAGE_HOME} link={LINK_PAGE_HOME} />
                <Link text={PAGE_BUILDINGS} link={LINK_PAGE_BUILDINGS} />
            </div>
            <p className={styles['copyright']}>&#169; {COPYRIGHT_TEXT}</p>
        </footer>
    );
});

export default Footer;
