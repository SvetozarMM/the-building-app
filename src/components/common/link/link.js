import React, { memo } from 'react';
import classnames from 'classnames';
import styles from './link.module.scss';
import { NavLink } from 'react-router-dom';

const Link = memo((props) => {
    const { className, text, link } = props;

    return (
        <div
            className={classnames(styles['link-wrapper'], {
                [className]: className
            })}
        >
            <NavLink className={styles['link']} to={link} activeClassName='active'>
                <p className={styles['label']}>{text}</p>
            </NavLink>
        </div>
    );
});

Link.defaultProps = {
    className: '',
    text: '',
    link: ''
};

export default Link;
