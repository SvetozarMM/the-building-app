import React, { memo } from 'react';
import styles from './logo.module.scss';

const Logo = memo(() => {
    return (
        <div className={styles['logo-wrapper']}>
            <p className={styles['text']}>B</p>
        </div>
    );
});

export default Logo;
