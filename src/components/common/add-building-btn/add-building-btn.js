import React, { useCallback, memo } from 'react';
import styles from './add-building-btn.module.scss';
import { Icon } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

const AddBuildingButton = memo((props) => {
    const { onClick } = props;

    const onClickHandler = useCallback(() => {
        onClick();
    }, [onClick]);

    return (
        <div className={styles['button-wrapper']} onClick={onClickHandler}>
            <Icon className={styles['icon-add']} component={AddIcon} />
            <p className={styles['label']}>Add Building</p>
        </div>
    );
});

AddBuildingButton.defaultProps = {
    onClick: () => {}
};

export default AddBuildingButton;
