import React, { useState, useCallback, useEffect } from 'react'
import classnames from 'classnames';
import { toLower } from 'ramda';
import styles from './input-box.module.scss';
import ErrorLabel from '../error-label/error-label';
import { Icon } from '@material-ui/core';
import { EditOutlined } from '@material-ui/icons';
import { BUILDING_INPUTS, UNAVAILABLE_NAME_MESSAGE } from '../../../constants/buildings';
import { validateInput } from '../../../utils/buildings'

const InputBox = (props) => {
    const {
      className,
      buildingProp,
      initialValue,
      buildingId,
      isCreateMode,
      unavailableNames,
      updateBuilding,
      isValidated,
      getValue,
    } = props;
    const [value, setValue] = useState('');
    const [isEditable, setIsEditable] = useState(false);
    const [isValidInput, setIsValidInput] = useState(true);

    useEffect(() => {
        setValue(initialValue);
    }, [initialValue]);

    useEffect(() => {
        if (isCreateMode) {
            setIsEditable(true);
        }
    }, [isCreateMode, initialValue]);

    useEffect(() => {
        isValidated(isValidInput);
    }, [isValidated, isValidInput]);

    useEffect(() => {
        return () => {
            setIsEditable(false);
            setIsValidInput(true);
        }
    }, [initialValue]);

    const onEditHandler = useCallback(() => {
        setIsEditable(true);
    }, []);

    const onValidateInput = useCallback((value) => {
        setIsValidInput(validateInput({
            inputType: buildingProp,
            text: value
        }) && !(unavailableNames.includes(toLower(value))));
    }, [buildingProp, unavailableNames]);

    const onChangeHandler = useCallback((e) => {
        const currentValue = e.target.value;

        setValue(currentValue);

        setTimeout(() => {
            onValidateInput(currentValue);
        }, 300);

        setTimeout(() => {
            getValue({
                buildingProp: buildingProp,
                value: currentValue
            });
        }, 300);
    }, [onValidateInput, getValue, buildingProp]);

    const onCancelEditHandler = useCallback(() => {
        setIsEditable(false);
        setIsValidInput(true);
        setValue(initialValue);
    }, [initialValue]);

    const onSaveEditHandler = useCallback(() => {
        setIsEditable(false);

        if (isValidInput) {
            updateBuilding({
                buildingId,
                updatedData: {
                    [buildingProp]: value
                }
            })
        } else {
            onCancelEditHandler();
            setIsValidInput(true);
        }
    }, [buildingId, value, buildingProp, isValidInput, updateBuilding, onCancelEditHandler]);

    return (
        <div
            className={classnames(styles['input-container'], {
                [className]: className,
                [styles['editable']]: isEditable
            })}
        >
            <div className={styles['input-box-wrapper']}>
                <div className={styles['input-box']}>
                    <p className={styles['label']}>{
                            isEditable && BUILDING_INPUTS[buildingProp].isEditable ?
                            `${BUILDING_INPUTS[buildingProp].placeholder} *` :
                            BUILDING_INPUTS[buildingProp].placeholder
                        }
                    </p>
                    <input
                        className={styles['input']}
                        type={BUILDING_INPUTS[buildingProp].type}
                        placeholder={BUILDING_INPUTS[buildingProp].placeholder}
                        value={value}
                        onChange={onChangeHandler}
                        disabled={!isEditable}
                    />
                    {unavailableNames.includes(toLower(value)) ? <ErrorLabel
                        isVisible={isEditable && toLower(value) !== toLower(initialValue)}
                        message={UNAVAILABLE_NAME_MESSAGE}
                        type="error"
                    /> :
                    <ErrorLabel
                        isVisible={!isValidInput}
                        message={BUILDING_INPUTS[buildingProp].errorMessage}
                        type="error"
                    />}
                </div>
                <Icon className={styles['icon-edit']} component={EditOutlined} onClick={onEditHandler} />
            </div>

            {!isCreateMode && isEditable ? <div className={styles['action-buttons']}>
                <p className={styles['btn-save']} onClick={onSaveEditHandler}>Save</p>
                <p className={styles['btn-cancel']} onClick={onCancelEditHandler}>Cancel</p>
            </div> : null}
        </div>
    );
}

InputBox.defaultProps = {
    className: '',
    buildingProp: '',
    initialValue: '',
    buildingId: null,
    isCreateMode: false,
    unavailableNames: [],
    updateBuilding: () => {},
    isValidated: () => {},
    getValue: () => {}
};

export default InputBox;
