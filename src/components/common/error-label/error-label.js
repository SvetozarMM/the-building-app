import React, { memo } from 'react'
import classnames from 'classnames';
import styles from './error-label.module.scss';

const ErrorLabel = memo((props) => {
    const { message, type, isVisible } = props;

    return (
        <div
            className={classnames(styles['label'], {
                [styles['visible']]: isVisible,
                [styles['error']]: type === 'error'
            })}
        >
            {message}
        </div>
    );
});

ErrorLabel.defaultProps = {
    message: '',
    type: '',
    isVisible: false
};

export default ErrorLabel;
