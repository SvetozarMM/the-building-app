import React, { memo } from 'react';
import { useLocation } from "react-router-dom";
import styles from './header.module.scss';
import { LINK_PAGE_BUILDINGS } from '../../constants/links';
import { WELCOME_TEXT, USERNAME } from '../../constants/texts';
import Logo from '../common/logo/logo';
import { Icon } from '@material-ui/core';
import { AccountCircle } from '@material-ui/icons';

const Header = memo((props) => {
    const { pathname } = useLocation();

    return (
        <header className={styles['header']}>
            <Logo />
            {pathname === LINK_PAGE_BUILDINGS ?
                <div className={styles['welcome-box']}>
                    <h3 className={styles['title']}>{WELCOME_TEXT}</h3>
                    <div className={styles['user-box']}>
                        <p className={styles['name']}>{USERNAME}</p>
                        <Icon className={styles['avatar']} component={AccountCircle} />
                    </div>
                </div>
            : null}
        </header>
    );
});

Header.defaultProps = {
    pathname: ''
}

export default Header;
