import React, { memo } from 'react';
import styles from './home.module.scss';
import { PAGE_BUILDINGS, PAGE_HOME_DESCRIPTION } from '../../constants/texts';
import { LINK_PAGE_BUILDINGS } from '../../constants/links';
import { IconFinger } from '../../assets/icons/icons';
import Link from '../common/link/link';

const Home = memo(() => {
    return (
        <div className={styles['home-container']}>
            <img className={styles['image']} alt="Buildings" src="/images/buildings-day.png" />
            <div className={styles['title-box']}>
                <Link className={styles['title']} text={PAGE_BUILDINGS} link={LINK_PAGE_BUILDINGS} />
                <div className={styles['icon']}>
                    <IconFinger />
                </div>
            </div>
            <p className={styles['description']}>{PAGE_HOME_DESCRIPTION}</p>
        </div>
    );
});

export default Home;
