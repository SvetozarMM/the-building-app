const validateName = (text) => text.trim().length < 3 || text.trim() === '' ? false : true;
const validateArea = (text) => text.trim() === '' ? false : true;

export const validateInput = ({ inputType, text }) => {
    switch (inputType) {
        case 'name': return validateName(text);
        case 'area': return validateArea(text);

        default: return true;
    }
};

export const randomIdGenerator = () => `_${Math.random().toString(36).substr(2, 9)}`;
export const getRandomImage = () => `img-${Math.floor(Math.random() * (10 - 1) + 1)}.png`;
