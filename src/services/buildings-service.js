import data from '../data/buildings.json';

const FETCH_BUILDINGS_DATA_DELAY = 500;

export const getAllBuildingRecords = () =>
    new Promise((resolve, reject) => setTimeout(() => resolve(data), FETCH_BUILDINGS_DATA_DELAY));
